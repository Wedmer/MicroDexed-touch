#ifndef microsynth_h_
#define microsynth_h_

#include "Arduino.h"

void microsynth_update_settings(uint8_t instance_id);
void braids_update_settings();
#endif